//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//


#ifndef _TILE_HH_
#define _TILE_HH_

#include <map>
#include <vector>
#include <iostream>
#include <cstdlib>

#define LENMAP (50)
#define HEIMAP (32)
#define NBTILE (LENMAP * HEIMAP)

enum	e_info
  {
    PO,
    SP,
    DEF,
    ATK
  };

class	Tile
{
public:
  Tile();
  Tile(int po, int sp, int def, int atk);
  virtual ~Tile();
public:
  void		  generateTile();
  std::vector<int> getInfo() const;
  std::map<int, int> getMap() const;
  int		   getDef() const;
  int		   getSp() const;
  int		   getPo() const;
  int		   getAtk() const;
  void		   setPo(int);
  void		   setAtk(int);
  void		   setSp(int);
  void		   setDef(int);

private:
  int		_atk;
  int		_po;
  int		_def;
  int		_sp;
  std::map<int, int>		_elemmap;
  std::vector<int>		_info;
};

#endif /*_TILE_HH_*/
