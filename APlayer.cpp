//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#include "APlayer.hh"

APlayer::APlayer(int type, int hp, int atk, int po, int sp) : _type(type), _hp(hp), _atk(atk), _po(po), _sp(sp)
{
  this->_x = 0;
  this->_y = 0;
  this->_team = 0;
  this->_def = 0;
  this->_isSelected = false;
  this->_hasMoved = false;
  this->_hasAttacked = false;
}

APlayer::~APlayer()
{
}

void	APlayer::setAtk(int i)
{
  this->_atk = i;
}

void	APlayer::setDef(int i)
{
  this->_def = i;
}

void	APlayer::setSp(int i)
{
  this->_sp = i;
}

void	APlayer::setPo(int i)
{
  this->_po = i;
}

void	APlayer::setAttacked(bool i)
{
  this->_hasAttacked = i;
}

void	APlayer::setHp(int i)
{
  this->_hp = i;
}

void	APlayer::setMove(bool i)
{
  this->_hasMoved = i;
}

void	APlayer::setSelect(bool i)
{
  this->_isSelected = i;
}

void	APlayer::setTeam(int i)
{
  this->_team = i;
}

void	APlayer::setX(int i)
{
  this->_x = i;
}

void	APlayer::setY(int i)
{
  this->_y = i;
}

bool	APlayer::getMove() const
{
  return (this->_hasMoved);
}

bool	APlayer::getSelect() const
{
  return (this->_isSelected);
}

int	APlayer::getHp() const
{
  return (this->_hp);
}

int	APlayer::getTeam() const
{
  return (this->_team);
}

int	APlayer::getX() const
{
  return (this->_x);
}

int	APlayer::getY() const
{
  return (this->_y);
}

int	APlayer::getAtk() const
{
  return (this->_atk);
}

int	APlayer::getPo() const
{
  return (this->_po);
}

int	APlayer::getSp() const
{
  return (this->_sp);
}

int	APlayer::getType() const
{
  return (this->_type);
}

bool	APlayer::getUltimate() const
{
  return (this->_isUltimate);
}

bool	APlayer::getAttacked() const
{
  return (this->_hasAttacked);
}

int	APlayer::getDef() const
{
  return (this->_def);
}

