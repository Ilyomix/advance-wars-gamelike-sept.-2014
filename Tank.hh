//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#ifndef _TANK_HH_
#define _TANK_HH_

#include "APlayer.hh"
#include <iostream>

class	Tank : public APlayer
{
public:
  Tank();
  virtual ~Tank();
public:
  void	move(int x, int y);
};

#endif /*!_TANK_HH_*/
