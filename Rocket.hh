//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#ifndef _ROCKET_HH_
#define _ROCKET_HH_

#include "APlayer.hh"
#include <iostream>

class	Rocket : public APlayer
{
public:
  Rocket();
  virtual ~Rocket();
public:
  void	move(int x, int y);
};

#endif /*!_ROCKET_HH_*/
