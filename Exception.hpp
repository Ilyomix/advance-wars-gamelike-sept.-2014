//
// Exception.hh for  in /home/demird_j/Backup2
// 
// Made by joseph demirdji
// Login   <demird_j@epitech.net>
// 
// Started on  Wed Aug  6 13:20:05 2014 joseph demirdji
// Last update Wed Aug  6 13:26:11 2014 joseph demirdji
//

#ifndef _EXCEPTION_HPP_
#define _EXCEPTION_HPP_

#include <exception>
#include <iostream>
#include <string>

class Exception : public std::exception
{
public:
  Exception(std::string const & type) throw();
  virtual ~Exception() throw();

public:
  virtual const char	*what() const throw();

protected:
  std::string	msg;
};

#endif /*!_EXCEPTION_HPP_*/
