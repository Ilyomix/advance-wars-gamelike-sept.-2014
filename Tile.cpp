//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#include "Tile.hh"

Tile::Tile()
{
  this->_po = 0;
  this->_sp = 0;
  this->_def = 0;
  this->_atk = 0;
}

Tile::Tile(int po, int sp, int def, int atk)
{
  this->_po = po;
  this->_sp = sp;
  this->_def = def;
  this->_atk = atk;
}

Tile::~Tile()
{
}

void			Tile::generateTile()
{
  int			randVal = 2;
  int			randValF = 2;

  for (unsigned int i = 0; i < NBTILE; i++)
    {
      randVal = rand() % 10;
      randVal = (randVal < 5) ? rand() % 2 : randVal;
      if (randVal >= 5)
	randVal = 2;
      this->_elemmap.insert(std::pair<int, int>(i, randVal));
    }
}

std::vector<int>	Tile::getInfo() const
{
  std::vector<int>	info;
  info.push_back(this->getDef());
  info.push_back(this->getPo());
  info.push_back(this->getSp());
  return (info);
}

std::map<int, int>	Tile::getMap() const
{
  return (this->_elemmap);
}

int			Tile::getDef() const
{
  return (this->_def);
}

int			Tile::getPo() const
{
  return (this->_po);
}

int			Tile::getSp() const
{
  return (this->_sp);
}

int			Tile::getAtk() const
{
  return (this->_atk);
}

void			Tile::setAtk(int atk)
{
  this->_atk = atk;
}

void			Tile::setSp(int sp)
{
  this->_sp = sp;
}

void			Tile::setPo(int po)
{
  this->_po = po;
}

void			Tile::setDef(int def)
{
  this->_def = def;
}
