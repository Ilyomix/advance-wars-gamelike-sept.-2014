//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#include "Gamecore.hh"

Gamecore::Gamecore()
{
  for (unsigned int i = 0; i < 2; i++)
    this->_coord.push_back(-1);
  for (unsigned int i = 0; i < 2; i++)
    this->_coordold.push_back(-1);
  this->_click = false;
  this->_turn = false;
  this->_unitName.push_back("Tank");
  this->_unitName.push_back("Berserk");
  this->_unitName.push_back("Lance-Missile");
}

Gamecore::~Gamecore()
{
}

void		Gamecore::drawMap(Tile *tile, sf::RenderWindow *app)
{
  this->_app = app;
  
  this->_tmpMap = tile->getMap();
  for (unsigned int i = 0; i < this->_tmpMap.size(); i++)
    {
      this->_map[this->_tmpMap[i]]->setPosition(sf::Vector2f((i % HMAP) * SIZETILE, (i / HMAP) * SIZETILE)); 
      this->_app->draw(*this->_map[this->_tmpMap[i]]);
    }
}

void		Gamecore::drawPlayers()
{
  for (unsigned int i = 0; i < this->_entities[1].size() * 2; i++)
    {
      this->_unit[this->_entities[(i / 6) + 1][i % 6]->getType() + (3 * (i / 6))]
       	->setPosition(sf::Vector2f(this->_entities[(i / 6) + 1][i % 6]->getX() * SIZETILE, 
      				   this->_entities[(i / 6) + 1][i % 6]->getY() * SIZETILE));

      if (this->_entities[(i / 6) + 1][i % 6]->getHp() > 0)
	this->_app->draw(*this->_unit[this->_entities[(i / 6) + 1][i % 6]->getType() + (3 * (i / 6))]);
      else
	{
	  this->_entities[(i / 6) + 1][i % 6]->setX(-1);
	  this->_entities[(i / 6) + 1][i % 6]->setY(-1);
	  this->_entities[(i / 6) + 1][i % 6]->setMove(true);
	  this->_entities[(i / 6) + 1][i % 6]->setAttacked(true);
	}
    }
}

void	Gamecore::saveCarac(int atk, int range, int speed, int def, int unit)
{
  std::vector<int> tmpCarac;

  tmpCarac.push_back(def);
  tmpCarac.push_back(atk);
  tmpCarac.push_back(speed);
  tmpCarac.push_back(range);
  this->_caracUnit.insert(std::pair<int, std::vector<int> >(unit, tmpCarac));
  return;
}
 
void	Gamecore::setClasses(std::vector<APlayer*> &s)
{  
  for (unsigned int i = 0; i < 2; i++)
    {
      Tank	*tank = new Tank();
      s.push_back(tank);
      if (i == 0)
	this->saveCarac(tank->getAtk(), tank->getPo(), tank->getSp(), tank->getDef(), TANK);
    }
  for (unsigned int i = 0; i < 2; i++)
    {
      Berserk	*bers = new Berserk();
      s.push_back(bers);
      if (i == 0)
	this->saveCarac(bers->getAtk(), bers->getPo(), bers->getSp(), bers->getDef(), BERSERK);
    }
  for (unsigned int i = 0; i < 2; i++)
    {
      Rocket	*rock = new Rocket();
      s.push_back(rock);
      if (i == 0)
	this->saveCarac(rock->getAtk(), rock->getPo(), rock->getSp(), rock->getDef(), ROCKET);
    }
}

void		Gamecore::initialTeam()
{
  int		team = 1, x = 0, y = 0;
  std::vector<APlayer*> s;
    
  this->setClasses(s);
  // Fill the entities in the map for the first player
  for (unsigned int i = 0; i < UNITPERTEAM; i++)
    {
      this->_units.push_back(s[i]);
      this->_units[i]->setX(5);
      this->_units[i]->setY((LMAP / 3) + i);	  	
      this->_units[i]->setTeam(1);
      this->_units[i]->setMove(false);
    }
  this->_entities.insert(std::pair<int, std::vector<APlayer *> >(1, this->_units));
  this->_units.clear();
  s.clear();

  this->setClasses(s);
  // Fill the entities in the map for the second player
  for (unsigned int i = 0; i < UNITPERTEAM; i++)
    {
      this->_units.push_back(s[i]);
      this->_units[i]->setX(HMAP - 5);
      this->_units[i]->setY(LMAP / 3 + i);	  	
      this->_units[i]->setTeam(2);
      this->_units[i]->setMove(true);
    }
  this->_entities.insert(std::pair<int, std::vector<APlayer *> >(2, this->_units));
  this->_units.clear();

  s.clear();
}

void			Gamecore::initialLib()
{
  sf::Texture		*img;
  sf::Sprite		*sprite;

  for (unsigned int i = 0; i < NBSPR; i++)
    {
      img = new sf::Texture();
      sprite = new sf::Sprite();
      if (!img->loadFromFile(PATHUNIT, sf::IntRect(SIZETILE * i, 0, SIZETILE, SIZETILE)))	
	throw (new Exception("Load Failure : Unable to load units sprites")); 
      this->_unit.push_back(sprite);
      this->_image.push_back(img);
      this->_unit[i]->setTexture(*this->_image[i]);
      std::cout << "Loading Units ............. (" << i + 1 << "/" << NBSPR << ")." << std::endl;  
    }
  this->_image.clear();
  for (unsigned int i = 0; i < NBMAP + 1; i++)
    {
      img = new sf::Texture();
      sprite = new sf::Sprite();
      if (!img->loadFromFile(PATHMAP, sf::IntRect(SIZETILE * i, 0, SIZETILE, SIZETILE)))
	throw (new Exception("Load Failure : Unable to load map sprites")); 
      this->_map.push_back(sprite);
      this->_image.push_back(img);
      this->_map[i]->setTexture(*this->_image[i]);
      std::cout << "Loading Map's tiles ....... (" << i + 1 << "/" << NBMAP + 1 << ")." << std::endl;  
    }
  this->_image.clear();
}

void			Gamecore::manageEvent(sf::Event eve)
{
  this->_elap = this->_time.getElapsedTime();
  if (eve.type == sf::Event::MouseMoved)
    {
      this->_map[ARROW]->setPosition(sf::Vector2f(eve.mouseMove.x - eve.mouseMove.x % SIZETILE, eve.mouseMove.y - eve.mouseMove.y % SIZETILE));
    }
  this->_app->draw(*this->_map[ARROW]);
  if (eve.type == sf::Event::MouseButtonPressed && this->_elap.asSeconds() > INPUTDELAY)
    {
      if (eve.mouseButton.button == sf::Mouse::Left)
	{
	  this->_coord[X] = eve.mouseButton.x / SIZETILE;
	  this->_coord[Y] = eve.mouseButton.y / SIZETILE;
	}
      if (!this->_click)
	{
	  this->_coordold[X] = this->_coord[X];
	  this->_coordold[Y] = this->_coord[Y];
	  this->selectEntities();
	}
      else
	this->moveEntities();
      this->_time.restart();
    }
}

void			Gamecore::selectEntities()
{
  for (unsigned int i = 0; i < this->_entities[1].size() * 2; i++)
    {
      if (this->_coord[X] == this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getX() &&
	  this->_coord[Y] == this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getY())
	{
	  this->_click = true;
	  this->_idSelect = i % UNITPERTEAM;
	  this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->setSelect(true);
	  break;
	}
    }
}

void			Gamecore::moveEntities()
{
  for (unsigned int i = 0; i < this->_entities[1].size() * 2; i++)
    {
      if (this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getSelect())
	{
	  this->_click = false;
	  if (this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getSp() >= abs(this->_coordold[X] - this->_coord[X]) + abs(this->_coordold[Y] - this->_coord[Y]) && this->checkCoord() && !this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getMove() && this->_turn == (i / UNITPERTEAM))
	    {
	      this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->move(this->_coord[X], this->_coord[Y]);
	      this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->setSelect(false);
	      this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->setMove(true);
	    }
	}
    }
}

bool			Gamecore::checkCoord()
{
  for (unsigned int i = 0; i < this->_entities[1].size() * 2; i++)
    {
      if (this->_coord[X] == this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getX() &&
	  this->_coord[Y] == this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getY())
	return (false);
    }
  return (true);
}

void			Gamecore::reset(int i)
{
  for (unsigned int e = 0; e < this->_entities[i].size(); e++)
    {
      this->_entities[i][e]->setMove(false);
      this->_entities[i][e]->setAttacked(false);
    }
}

void			Gamecore::checkTileBonus()
{
  int			x, y;

  this->_elapRef = this->_timeRef.getElapsedTime();
  if (this->_elapRef.asSeconds() > REFRESHDELAY)
    {
      std::cout << "\033[2J" << std::endl;
      std::cout << "\033[1;38m[	ShootAndRun V 1.0.0 -   StatusBoard	]\033[0;0m\n" << std::endl;
      if (!this->_turn)
	std::cout << "\033[1;38m[Turn >>]	Turn for Blue team \033[0;0m\n" << std::endl;
      else
	std::cout << "\033[1;38m[Turn >>]	Turn for Red team \033[0;0m\n" << std::endl;
    }
  for (unsigned int i = 0; i < this->_entities[1].size() * 2; i++)
    {
      x = this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getX();
      y = this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getY();

      this->setTileBonus(this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM], 
			  this->_tmpMap[(y * HMAP) + x]);
      
      if (this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getHp() > 0 && this->_elapRef.asSeconds() > REFRESHDELAY)
	{
	  if (i == 0)
	    std::cout << "\033[1;34m" << "###################### Team Blue #####################\033[1;38m\n" << std::endl;

	  if (i % 6 == 0 && i)
	    std::cout << "\033[1;31m" << "###################### Team Red  ####################\033[1;38m\n" << std::endl;

	  if (i / 6 == 0)
	    std::cout << "\033[1;34m" << "Unité N°" << (i % 6 + 1) << "\033[1;38m" << std::endl;
	  else
	    std::cout << "\033[1;31m" << "Unité N°" << (i % 6 + 1) << "\033[1;38m" << std::endl;

	  if (this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getMove() || this->_turn + 1 != (i / UNITPERTEAM) + 1)
	    std::cout << this->_unitName[this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getType()] << " [Status: BUSY] >>\n";
	  else if (!this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getMove())
	    std::cout << this->_unitName[this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getType()] << " [Status: READY] >>\n";

	  std::cout << "\033[1;31m" << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getHp() << " HP |\033[0;0m" 
		    << "\033[1;32m Range " << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getPo() << " |\033[0;0m"
		    << "\033[1;33m Speed " << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getSp() << " |\033[0;0m"
		    << "\033[1;34m Attack " << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getAtk() << " |\033[0;0m" 
		    << "\033[1;35m Defense " << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getDef() << " |\033[0;0m"
		    << "\033[1;38m Location (" << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getX() << ", " << this->_entities[(i / UNITPERTEAM) + 1][i % UNITPERTEAM]->getY() << ")" << " | \033[0;0m" << std::endl;
	  std::cout << std::endl;
	  this->_timeRef.restart();
	}
    }
}

void			Gamecore::setTileBonus(APlayer *a, int i)
{
  Tile	*mount = new Tile(1, -2, 0, 0);
  Tile	*river = new Tile(0, -1, 1, 0);
  Tile	*plain = new Tile(0, 0, 1, 1);

  std::vector<Tile*> tiles;
  
  tiles.push_back(mount);
  tiles.push_back(river);
  tiles.push_back(plain);

  a->setDef(this->_caracUnit[a->getType()][DEF_U] + tiles[i]->getDef());
  a->setAtk(this->_caracUnit[a->getType()][ATK_U] + tiles[i]->getAtk());
  a->setSp(this->_caracUnit[a->getType()][SP_U] + tiles[i]->getSp());
  a->setPo(this->_caracUnit[a->getType()][PO_U] + tiles[i]->getPo());

  delete (mount);
  delete (river);
  delete (plain);
}

void		        Gamecore::checkRange()
{
  if (!this->_turn)
    for (unsigned int i = 0; i < this->_entities.size(); i++)
      {
	for (unsigned int e = 0; e < this->_entities.size(); e++)
	  {
	    if (this->_entities[1][i]->getPo() < abs(this->_entities[2][e]->getX() - this->_entities[1][i]->getX()) 
		+ abs(this->_entities[2][e]->getY() - this->_entities[1][i]->getY()))
	      this->_entities[1][i]->setAttacked(true);
	  }
      }
  else
    for (unsigned int i = 0; i < this->_entities.size(); i++)
      {
	for (unsigned int e = 0; e < this->_entities.size(); e++)
	  {
	    if (this->_entities[2][i]->getPo() < abs(this->_entities[1][e]->getX() - this->_entities[2][i]->getX()) 
		+ abs(this->_entities[1][e]->getY() - this->_entities[2][i]->getY()))
	      this->_entities[2][i]->setAttacked(true);
	  }
      }
}

void			Gamecore::makeAttack(sf::Event eve)
{
  int			x, y;

  if ((this->_entities[1].size() < this->_idSelect || this->_idSelect < 0) 
      || (this->_entities[2].size() < this->_idSelect || this->_idSelect < 0))
    return;

  if (eve.type == sf::Event::MouseButtonPressed && this->_elap.asSeconds() > INPUTDELAY)
    {
      if (eve.mouseButton.button == sf::Mouse::Left)
	{
	  x = eve.mouseButton.x / SIZETILE;
	  y = eve.mouseButton.y / SIZETILE;
	  if (this->_turn)
	    {
	      for (unsigned int i = 0; i < this->_entities[1].size(); i++)
		if (this->_entities[1][i]->getX() == x && 
		    this->_entities[1][i]->getY() == y) 
		  {
		    if (this->_entities[1][i]->getPo() >= abs(this->_coord[X] - x) + abs(this->_coord[Y] - y)
			&& !this->_entities[1][i]->getAttacked())
		      {
			this->_entities[1][i]->setHp(this->_entities[1][i]->getHp() - 
						     (this->_entities[2][this->_idSelect]->getAtk() - 
						      this->_entities[1][i]->getDef()));
			this->_entities[1][i]->setAttacked(true);
		      }
		  }
	    }
	  else
	    {
	      for (unsigned int i = 0; i < this->_entities[1].size(); i++)
		if (this->_entities[2][i]->getX() == x && 
		    this->_entities[2][i]->getY() == y) 
		  {
		    if (this->_entities[2][i]->getPo() >= abs(this->_coord[X] - x) + abs(this->_coord[Y] - y) 
			&& !this->_entities[2][i]->getAttacked())
		      {
			this->_entities[2][i]->setHp(this->_entities[2][i]->getHp() - 
						     (this->_entities[1][this->_idSelect]->getAtk() - 
						      this->_entities[2][i]->getDef()));
			this->_entities[2][i]->setAttacked(true);
		      }
		  }
	    }
	}
      this->_time.restart();
    }
}

void			Gamecore::checkTurn()
{
  // Check turn for team one;
  for (unsigned int i = 0; i < this->_entities[1].size(); i++)
    {
      if (!this->_entities[1][i]->getMove() && !this->_entities[1][i]->getAttacked())
	break;
      if (i == UNITPERTEAM - 1)
	{
	  reset(1);
	  this->_turn = true;
	}
    }

  // Check turn for team two;
  for (unsigned int i = 0; i < this->_entities[1].size(); i++)
    {
      if (!this->_entities[2][i]->getMove() && !this->_entities[1][i]->getAttacked())
	break;
      if (i == UNITPERTEAM - 1)
	{
	  reset(2);
	  this->_turn = false;
	}
    }
}

int			main(int ac, char **av)
{
  try
    {
      Gamecore		core;
      Tile			*tile = new Tile();
      sf::RenderWindow      *app = new sf::RenderWindow(sf::VideoMode(LENWIN, HEIWIN, BPP), WINDOWNAME, sf::Style::Close); 
      sf::Event eve;
      sf::SoundBuffer	buff;
      sf::Sound		sound;
      std::stringstream	ss;
      short			aleaMusic;
      
      srand(time(NULL));
      
      aleaMusic = rand() % 2 + 1;
      ss << aleaMusic;

      if (!buff.loadFromFile("./Sounds/theme" + ss.str() + ".ogg"))
	throw (new Exception("Load Failure : Unable to load sounds")); 
      else
	std::cout << "Loading Sounds ........... " << "./Sounds/theme" + ss.str() + ".ogg" << std::endl;
      sound.setBuffer(buff);
      
      sound.play();

      core.initialLib();
      core.initialTeam();
      tile->generateTile();
      while (app->isOpen())
	{
	  while (app->pollEvent(eve))
	    {
	      if ((eve.type == sf::Event::Closed || eve.key.code == sf::Keyboard::Escape) 
		  && eve.type != sf::Event::MouseMoved)
		app->close();
	    }
	  app->clear();
	  core.drawMap(tile, app);
	  core.drawPlayers();
	  core.checkTileBonus();
	  core.manageEvent(eve);
	  core.makeAttack(eve);
	  core.checkRange();
	  core.checkTurn();
	  app->display();
	}
    }
  catch (std::exception *e)
    {
      std::cerr << "Exception : " << e->what() << std::endl;
    }
  return (SUCCESS);
}
