//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#include "Exception.hpp"

Exception::Exception(const std::string & type) throw() : msg(type)
{
}

Exception::~Exception() throw()
{
}

const char	*Exception::what() const throw()
{
  return (this->msg.data());
}
