//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#ifndef _GAMECORE_HH_
#define _GAMECORE_HH_

/* Includes */

#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include "./SFML-2.0/include/SFML/Audio.hpp"
#include "./SFML-2.0/include/SFML/Graphics.hpp"
#include "./SFML-2.0/include/SFML/System.hpp"
#include "./SFML-2.0/include/SFML/Window.hpp"
#include "Tile.hh"
#include "Tank.hh"
#include "Rocket.hh"
#include "Berserk.hh"
#include "Exception.hpp"

/* Defines */

#define SUCCESS	   (255)

#define SIZETILE   (32)
#define PATHMAP	   ("./Assets/tile.png")
#define PATHUNIT   ("./Assets/sprite.png")
#define WINDOWNAME ("ShootandRun")
#define LENWIN	   (1600)
#define HEIWIN	   (896)
#define NBSPR	   (6)
#define NBPIC	   (6)
#define NBMAP	   (3)
#define BPP	   (32)
#define LMAP	   (28)
#define HMAP	   (50)
#define UNITPERTEAM (6)
#define INPUTDELAY  (0.45)
#define REFRESHDELAY (1.00)
/* Enums */

enum	e_coord
  {
    X,
    Y
  };

enum	e_unit
  {
    TANK,
    BERSERK,
    ROCKET
  };

enum	e_stats
  {
    DEF_U,
    ATK_U,
    SP_U,
    PO_U
  };

enum	e_map
  {
    WATER,
    PLAIN,
    MOUNT,
    ARROW
  };


class		Gamecore 
{
public:
  Gamecore();
  virtual ~Gamecore();
public:
  void	loadSprite();
  void	update();
  void	initialLib();
  void	setClasses(std::vector<APlayer*> &);
  void	selectEntities();
  void	reset(int);
  void	moveEntities();
  bool	checkCoord();
  void	checkRange();
  void	checkTurn();
  void	saveCarac(int, int, int, int, int);
  void	checkTileBonus();
  void	setTileBonus(APlayer*, int);
  void	makeAttack(sf::Event);
  // void printStatus();
  virtual void	initialTeam();
  virtual void	drawMap(Tile* tile, sf::RenderWindow*);
  virtual void	drawPlayers();
  void	manageEvent(sf::Event);
protected:
  sf::Time		_elap;
  sf::Clock		_time;
  sf::Clock		_timeRef;
  sf::Time		_elapRef;
  sf::RenderWindow	*_app;
  std::map<int, std::vector<APlayer*> > _entities;
  std::map<int, int>			_tmpMap;
  std::map<int, std::vector<int> >  	_caracUnit;
  std::vector<APlayer*>		_units;
  std::vector<sf::Sprite*>	_unit;
  std::vector<sf::Sprite*>	_map;
  std::vector<sf::Texture*>	_image;
  std::vector<int>		_coord;
  std::vector<int>		_coordold;
  std::vector<std::string>	_unitName;
  int				_idSelect;
  bool				_turn;
  bool				_click;
  bool				_mouseUsed;
};

#endif /*!_GAMECORE_HH*/
