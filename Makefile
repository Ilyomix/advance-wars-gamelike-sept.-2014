##
## Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
## <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
##

SRC	 =	Gamecore.cpp \
		Tile.cpp \
		APlayer.cpp \
		Tank.cpp \
		Berserk.cpp \
		Rocket.cpp \
		Exception.cpp
 
GPP	 =	g++

##CPPFLAGS +=	-W -Wall -Wextra -g3

CPPFLAGS +=	-I./SFML-2.0/include -O3

LIBS	 =	./SFML-2.0/lib/

LIB	 =	-Wl,-rpath-link=$(LIBS)

LDFLAGS	 =	-L$(LIBS) -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

NAME	=	shootandrun

OBJ	=	$(SRC:.cpp=.o)

$(NAME):	$(OBJ)
	$(GPP) -o $(NAME) $(OBJ) $(LDFLAGS)

all:		$(NAME)

clean:
	rm -f $(OBJ)
	rm -f *~

fclean:		clean
	rm -f $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
