//
// Gamecore.hh for Ilyes Abd-Lillah - EPITECH 2017 - Started on  Mon Aug 4, 2014 - 16:35:26
// <* THIS CODE is maded for you by Ilyes Abd-Lillah <---> Anyone who use it had to mention this in his header >
//

#ifndef _APLAYER_HH_
#define _APLAYER_HH_

#include <iostream>
#include <string>

class	APlayer
{
public:
  APlayer(int type, int hp, int atk, int po, int sp);
  virtual ~APlayer();
  virtual void	   setAtk(int);
  virtual void	   setSp(int);
  virtual void	   setDef(int);
  virtual void	   setPo(int);
  virtual void	   setX(int);
  virtual void	   setY(int);
  virtual void	   setTeam(int);
  virtual void	   setMove(bool);
  virtual void	   setHp(int);
  virtual void	   setAttacked(bool);
  virtual void	   setSelect(bool);
  virtual bool	   getSelect() const;
  virtual int	   getTeam() const;
  virtual int	   getHp() const;
  virtual int	   getAtk() const;
  virtual int	   getPo() const;
  virtual int	   getSp() const;
  virtual int	   getDef() const;
  virtual int	   getType() const;
  virtual int	   getX() const;
  virtual int	   getY() const;
  virtual bool	   getUltimate() const;
  virtual bool	   getMove() const;
  virtual bool	   getAttacked() const;
  virtual void	   move(int, int) = 0;
protected:
  int	  _team;
  int	  _def;
  int	  _x;
  int	  _y;
  int	  _hp;
  int	  _atk;
  int	  _po;
  int	  _sp;
  int	  _type;
  bool	  _hasAttacked;
  bool	  _isUltimate;
  bool	  _isSelected;
  bool	  _hasMoved;
};

#endif /*!_APLAYER_HH_*/
